package com.izabel.spring.estudo.entity;

import com.izabel.spring.estudo.enums.TopicStatusEnum;

public class Topic implements IEntity<Long>{

	private static final long serialVersionUID = 7637452343622876923L;

	private Long id;
	private String sumary;
	private TopicStatusEnum status;

	@Override
    public Long getId() {
		return id;
    }

	@Override
    public void setId(final Long id) {
		this.id = id;
    }

    public String getSumary() {
    	return sumary;
    }

    public void setSumary(final String sumary) {
    	this.sumary = sumary;
    }

    public TopicStatusEnum getStatus() {
    	return status;
    }

    public void setStatus(final TopicStatusEnum status) {
    	this.status = status;
    }

}
