package com.izabel.spring.estudo.entity;

import java.io.Serializable;

public interface IEntity<K extends Serializable> extends Serializable {

	K getId();

	void setId(final K id);

}
