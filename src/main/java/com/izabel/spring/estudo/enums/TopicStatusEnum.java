package com.izabel.spring.estudo.enums;

public enum TopicStatusEnum implements IEnum<String> {

	PROGRESS("P", "andamento"), NEW("N", "novo");

	private String value;
	private String label;

	private TopicStatusEnum(String value, String label) {
		this.value = value;
		this.label = label;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String getLabel() {
		return label;
	}

}
