package com.izabel.spring.estudo.enums;

public interface IEnum<T> {

	T getValue();

	String getLabel();

}
