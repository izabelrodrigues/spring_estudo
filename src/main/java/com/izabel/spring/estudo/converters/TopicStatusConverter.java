package com.izabel.spring.estudo.converters;

import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

import com.izabel.spring.estudo.enums.TopicStatusEnum;

@FacesConverter("topicStatusConverter")
public class TopicStatusConverter extends EnumConverter{

	public TopicStatusConverter() {
	    super(TopicStatusEnum.class);
    }

}
